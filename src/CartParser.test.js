import CartParser from './CartParser';
import { readFileSync } from 'fs';
jest.mock('fs');
let parser;

beforeEach(() => {
    parser = new CartParser();
});

describe('CartParser - unit tests', () => {

    it("should return an error if header value is wrong", () => {
        const csvFileText =
            "Not a Product name,Price,Quantity\n"

        const expected = [{
            type: "header",
            column: 0,
            row: 0,
            message: "Expected header to be named \"Product name\" but received Not a Product name.",
        }]

        expect(parser.validate(csvFileText)).toEqual(expected);
    });

    it("should return an error if header Price value is not provided", () => {
        const csvFileText =
            "Product name,Quantity\n"

        const expected = [{
                type: "header",
                column: 1,
                row: 0,
                message: "Expected header to be named \"Price\" but received Quantity.",
            },
            {
                type: "header",
                column: 2,
                row: 0,
                message: "Expected header to be named \"Quantity\" but received undefined.",
            }
        ]

        expect(parser.validate(csvFileText)).toEqual(expected);
    });

    it("should return an error if cell is not а string but empty", () => {
        const csvFileText =
            "Product name,Price,Quantity\n" +
            ",9.00,2";

        const expected = [{
            type: "cell",
            column: 0,
            row: 1,
            message: "Expected cell to be a nonempty string but received \"\"."
        }];

        expect(parser.validate(csvFileText)).toEqual(expected);
    });

    it("should return an error if number cell contain no number but string", () => {
        const csvFileText =
            "Product name,Price,Quantity\n" +
            "Mollis consequat,string,2";

        const expected = [{
            type: "cell",
            column: 1,
            row: 1,
            message: "Expected cell to be a positive number but received \"string\"."
        }];

        expect(parser.validate(csvFileText)).toEqual(expected);
    });

    it("should return an error when schema.columns.length > cells in the body line", () => {
        const csvFileText =
            "Product name,Price,Quantity\n" +
            "Mollis consequat,9.00";

        const expected = [{
            type: "row",
            column: -1,
            row: 1,
            message: "Expected row to have 3 cells but received 2."
        }];

        expect(parser.validate(csvFileText)).toEqual(expected);
    });

    it("should return no error when schema.columns.length < cells in the body line", () => {
        const csvFileText =
            "Product name,Price,Quantity\n" +
            "Mollis consequat,9.00,2";

        const expected = [];

        expect(parser.validate(csvFileText)).toEqual(expected);
    });

    it("should return an error if cell that must contain the positive number has negative value", () => {
        const csvFileText =
            "Product name,Price,Quantity\n" +
            "Mollis consequat,9.00,-2";

        const expected = [{
            type: "cell",
            column: 2,
            row: 1,
            message: "Expected cell to be a positive number but received \"-2\"."
        }];

        expect(parser.validate(csvFileText)).toEqual(expected);
    })

    it("should not return an error when the body is empty", () => {
        const csvFileText =
            "Product name,Price,Quantity";

        const expected = [];

        expect(parser.validate(csvFileText)).toEqual(expected);
    })

    it("should return total price for cart items", () => {
        const cart = [{
                id: "3e6def17-5e87-4f27-b6b8-ae78948523a9",
                name: "Mollis consequat",
                price: 9.00,
                quantity: 2
            },
            {
                id: "0d1cbe5e-3de6-4f6a-9c53-bab32c168fbf",
                name: "Condimentum aliquet",
                price: 13.90,
                quantity: 1
            }
        ]

        const expected = 31.9;

        expect(parser.calcTotal(cart)).toBeCloseTo(expected);
    })

});

describe('CartParser - integration test', () => {

    it("should throw an error on failed validation", () => {
        const csvFileText =
            "Product name,Price,Not expected field\n" +
            "Mollis consequat,9.00,2\n" +
            "Tvoluptatem,10.32,1\n" +
            "Scelerisque lacinia,18.90,1\n" +
            "Consectetur adipiscing,28.72,-10\n" +
            "Condimentum aliquet,1";

        readFileSync.mockReturnValueOnce(csvFileText);

        const expected = "Validation failed!";

        expect(() => parser.parse("")).toThrow(expected);
    })

});